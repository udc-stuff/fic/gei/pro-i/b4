#include <stdio.h>

// void get_data(int a[], int size) {
void get_data(int *a, int size) {
  int cnt;
  for (cnt = 0; cnt < size; cnt++) {
    printf("Inserte el numero %d/%d: ", cnt + 1, size);
    scanf("%d", &a[cnt]);
  }
}

//void print_data(int a[], int size) {
void print_data(int *a, int size) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    printf("%s%d%s", (cnt == 0?"[":""), a[cnt], (cnt == size - 1?"]\n":", ") );
}

int main() {
  const int SIZE = 10;
  int array[SIZE];

  get_data(array, SIZE);
  print_data(array, SIZE);

  return 0;
}
