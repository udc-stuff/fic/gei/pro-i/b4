#include <stdio.h>

void get_data(int animals, int max, char a[animals][max]) {
  int cnt;
  for (cnt = 0; cnt < animals; cnt++) {
    printf("Inserte el animal %d/%d: ", cnt + 1, animals);
    scanf("%s", a[cnt]);
  }
}

void print_data(int animals, int max, char a[animals][max]) {
  int cnt;

  for (cnt = 0; cnt < animals; cnt++)
    printf("%s'%s'%s", (cnt == 0?"[":""), a[cnt], (cnt == animals - 1?"]\n":", ") );
}

int main() {
  const int ANIMALS = 7;
  const int MAX = 50;
  char array[ANIMALS][MAX];

  get_data(ANIMALS, MAX, array);
  print_data(ANIMALS, MAX, array);

  return 0;
}
