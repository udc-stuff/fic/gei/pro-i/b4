#include <stdio.h>
#include <stdbool.h>

int get_int(int min, int max) {
  int tmp = min - 1;

  while (tmp < min || tmp > max) {
    printf("Inserte un numero entre %d y %d: ", min, max);
    scanf("%d", &tmp);
  }

  return tmp;
}

void get_data(float *a, int size) {
  int cnt;
  for (cnt = 0; cnt < size; cnt++) {
    printf("Inserte el numero %d/%d: ", cnt + 1, size);
    scanf("%f", &a[cnt]);
  }
}

void print_data(float *a, int size) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    printf("%s%.2f%s", (cnt == 0?"[":""), a[cnt], (cnt == size - 1?"]\n":", ") );
}

float get_sum(float *a, int size) {
  int cnt;
  float total = 0.0f;

  for (cnt = 0; cnt < size; cnt++)
    total += a[cnt];

  return total;
}

int main() {
  const int MAX = 30;
  float array[MAX];
  float total;

  int size = get_int(1, MAX);

  get_data(array, size);
  print_data(array, size);

  printf("La suma de los elementos introducidos es: %.2f\n", get_sum(array, size));

  return 0;
}
