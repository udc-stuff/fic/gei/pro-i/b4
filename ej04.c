#include <stdio.h>

char *get_dayname(int day) {
  switch (day) {
    case 0: return "Lunes";
    case 1: return "Martes";
    case 2: return "Miercoles";
    case 3: return "Jueves";
    case 4: return "Viernes";
    case 5: return "Sabado";
    case 6: return "Domingo";
    default: return "Invalido";
  }
}

void get_data(float *a, int size) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++) {
    printf("Inserte las horas de estudio del dia %s: ", get_dayname(cnt));
    scanf("%f", &a[cnt]);
  }
}

void print_data(float *a, int size) {
  int cnt;

  printf("\nHoras de estudio:\n");
  for (cnt = 0; cnt < size; cnt++)
    printf("\t%s: %.2f\n", get_dayname(cnt), a[cnt]);

  printf("\n");
}

int get_max(float *a, int size) {
  int cnt;
  int aux = 0;

  for (cnt = 1; cnt < size; cnt++)
    if (a[cnt] > a[aux])
      aux = cnt;

  return aux;
}

int get_min(float *a, int size) {
  int cnt;
  int aux = 0;

  for (cnt = 1; cnt < size; cnt++)
    if (a[cnt] < a[aux])
      aux = cnt;

  return aux;
}

float get_avg(float *a, int size) {
  int cnt;
  float aux = 0.0f;

  for (cnt = 0; cnt < size; cnt++)
      aux += a[cnt];

  return aux/size;
}

int main() {
  const int SIZE = 7;
  float array[SIZE];

  float avg;
  int min, max;

  get_data(array, SIZE);

  min = get_min(array, SIZE);
  max = get_max(array, SIZE);
  avg = get_avg(array, SIZE);

  print_data(array, SIZE);
  printf("La media de horas de estudio es %.1f\n", avg);
  printf("El dia menos productivo fue el %s con %.1f horas de estudio\n", get_dayname(min), array[min]);
  printf("El dia mas productivo fue el %s con %.1f horas de estudio\n", get_dayname(max), array[max]);

  return 0;
}
