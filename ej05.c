#include <stdio.h>

void get_data(float *a, int size) {
  int cnt;
  for (cnt = 0; cnt < size; cnt++) {
    printf("Inserte la temperatura que hubo a las %02d horas: ", cnt);
    scanf("%f", &a[cnt]);
  }
}

void print_data(float *a, int size) {
  int cnt;

  printf("\nLas temperaturas registradas son:\n");
  for (cnt = 0; cnt < size; cnt++)
    printf("\t%02dh: %.2f grados\n", cnt, a[cnt] );
  printf("\n");
}

int get_max(float *a, int size) {
  int cnt;
  int aux = 0;

  for (cnt = 1; cnt < size; cnt++)
    if (a[cnt] > a[aux])
      aux = cnt;

  return aux;
}

int get_min(float *a, int size) {
  int cnt;
  int aux = 0;

  for (cnt = 1; cnt < size; cnt++)
    if (a[cnt] < a[aux])
      aux = cnt;

  return aux;
}

float get_avg(float *a, int size) {
  int cnt;
  float aux = 0.0f;

  for (cnt = 0; cnt < size; cnt++)
      aux += a[cnt];

  return aux/size;
}
int main() {
  const int SIZE = 24;
  float array[SIZE];

  int min, max;
  float avg;

  get_data(array, SIZE);
  print_data(array, SIZE);

  min = get_min(array, SIZE);
  max = get_max(array, SIZE);
  avg = get_avg(array, SIZE);

  printf("La temperatura media fue %.1f\n", avg);
  printf("La temperatura minima de %.1f grados fue registrada a las %02d.\n", array[min], min);
  printf("La temperatura maxima de %.1f grados fue registrada a las %02d.\n", array[max], max);

  return 0;
}
