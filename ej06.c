#include <stdio.h>

void get_data(int days, int hours, float m[][hours]) {
  int d, h;

  for (d = 0; d < days; d++) {
    printf("Dia %d\n", d);
    for (h = 0; h < hours; h++) {
      printf("\tInserte la temperatura que hubo a las %02d horas el dia %d: ", h, d);
      scanf("%f", &m[d][h]);
    }
  }
}

void print_data(int days, int hours, float m[][hours]) {
  int d, h;

  for (d = 0; d < days; d++) {
    printf("Las temperaturas registradas el dia %d son:\n", d);
    for (h = 0; h < hours; h++) {
      printf("\t%02dh: %.2f grados\n", h, m[d][h]);
    }
  }

}

float get_avg(float *a, int size) {
  int cnt;
  float aux = 0.0f;

  for (cnt = 0; cnt < size; cnt++)
      aux += a[cnt];

  return aux/size;
}

float get_avg_by_column(int rows, int columns, float m[][columns], int index) {
  int cnt;
  float aux = 0.0f;

  for (cnt = 0; cnt < rows; cnt++)
    aux += m[cnt][index];

  return aux/rows;
}

int main() {
  const int HOURS = 24;
  const int DAYS = 7;
  float matrix[DAYS][HOURS];

  int cnt;
  float avg;

  get_data(DAYS, HOURS, matrix);
  print_data(DAYS, HOURS, matrix);

  for (cnt = 0; cnt < DAYS; cnt++) {
    avg = get_avg(&matrix[cnt][0], HOURS);
    printf("Temperatura media el dia %d: %.1f\n", cnt, avg);
  }

  for (cnt = 0; cnt < HOURS; cnt++) {
    avg = get_avg_by_column(DAYS, HOURS, matrix, cnt);
    printf("Temperatura media de la hora %2d: %.1f\n", cnt, avg);
  }

  printf("Temperatura media total: %.1f\n", get_avg(matrix, DAYS * HOURS));

  return 0;
}
