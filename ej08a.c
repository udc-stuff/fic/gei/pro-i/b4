#include <stdio.h>

int get_data(int *a, int size) {
  int cnt;
  char tmp = ' ';

  printf("Introduza los elementos del array separados por espacios, <enter> para terminar:\n");
  for (cnt = 0; cnt < size && tmp != '\n'; cnt++) {
    scanf("%d", &a[cnt]);
    scanf("%c", &tmp);
  }

  while (tmp != '\n') {
    scanf("%c", &tmp);
  }

  return cnt;
}

void foo(int *a, int size, int element, int *min, int *max) {
  int cnt;

  *min = *max = -1;
  for (cnt = 0; cnt < size; cnt++)
    if (a[cnt] == element) {
      if (*min == -1)
        *min = *max = cnt;
      else
        *max = cnt;
    }
}

int main() {
  const int MAX = 20;
  int size;
  int array[MAX];

  int min, max, number;

  size = get_data(array, MAX);
  
  printf("Introduzca el numero a buscar: ");
  scanf("%d", &number);

  foo(array, size, number, &min, &max);
  if (min == -1)
    printf("El numero %d no aparece en el array\n", number);
  else if (min == max)
    printf("El numero %d aparece por primera y unica vez en la posicion %d\n", number, min);
  else
    printf("El numero %d aparece por primera vez en la posicion %d y por ultima vez en la posicion %d\n", number, min, max);

  return 0;
}
