#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

void print_data(int *a, int size) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    printf("%s%d%s", (cnt == 0?"[":""), a[cnt], (cnt == size - 1?"]\n":", ") );
}

/**
 * Esta funcion lee una cadena de entrada repleta de numeros y los introduce
 * dentro de un array
 * str: cadena de texto que contiene los numeros
 * a: array donde almacenar los numeros
 * size: tamano maximo del array
 *
 * return la funcion devuelve la cantidad de numeros que se han introducido
 *  dentro del array
 */
int str_to_ints(char *str, int *a, int size) {
	char delim[] = {" \t\n"}; // separadores que se usan dentro de str
	char *token;

	int cnt = 0;

	while ((token = strsep(&str, delim)) != NULL) {
		//printf("TOKEN %d) <%s>\n", cnt, token);
		if (strcmp(token, "")) {
			a[cnt] = strtol(token, NULL, 10);
			cnt++;
		}

		if (cnt == size) // No se pueden almacenar mas numeros dentro del array
			return cnt;

	}

	return cnt;
}

/**
 * Esta funcion trata de buscar un numero dentro de un array de numeros de tal
 * manera que devuelve el primer y el ultimo elemento encontrado
 * x: elemento a buscar dentro del array
 * a: array donde estan los numeros
 * size: tamano del array
 * result: array de dos posiciones donde se almacenan los resultados
 * la posicion 0 indica donde esta el primer elemento
 * la posicion 1 indica donde esta el ultimo elemento
 * el valor -1 en results indica que no se ha encontrado un indice que
 * cumpla con la condicion
 */
void search_first_last(int x, int *a, int size, int *result) {
	int cnt;

	result[0] = result[1] = -1;

	for (cnt = 0; cnt < size; cnt++) {
		if (x == a[cnt]) {
			if (result[0] == -1)
				result[0] = cnt;
			else
				result[1] = cnt;
		}
	}

}


void ask_to_search(int *a, int size) {
	const int MAX_STR = 10;
	char buffer[MAX_STR];

	char *ptr;

	int result[2];
	int x;

	bool stop = false;

	while (!stop) {
		printf("Introduzca el numero que desea buscar.\n(Para finalizar pulse enter)\n");
		fgets(buffer, MAX_STR, stdin);
		if ((ptr = strchr(buffer, '\n'))!=NULL)
			*ptr = '\0';

		if (!strcmp(buffer, "")) {
			stop = true;
		} else {
			x = strtol(buffer, NULL, 10);
			search_first_last(x, a, size, result);

			if (result[0] == -1)
				printf("El numero %d no aparece en el array.\n", x);
			else if (result[1] == -1)
				printf("El numero %d aparece por primera y unica vez en la posicion %d\n", x, result[0]);
			else
				printf("El numero %d aparece por primera vez en la posicion %d y por ultima vez en la posicion %d\n", x, result[0], result[1]);
		}
	}

}

int main() {
	const int MAX_NUM = 15;
	const int MAX_STR = 50;
	int numbers[MAX_NUM];
	char buffer[MAX_STR];
	int size;

	printf("Introduce los numeros que formaran parte del array (MAX %d).\n"
				 "Los números han de estar separados por espacios.\n"
				 "Para finalizar pulse <enter>: \n", MAX_NUM);
	fgets(buffer, MAX_STR, stdin);

	size = str_to_ints(buffer, numbers, MAX_NUM);

	printf("El array de numeros es: ");
	print_data(numbers, size);

	ask_to_search(numbers, size);

	return 0;
}
