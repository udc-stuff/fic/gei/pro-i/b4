#include <stdio.h>
#include <math.h>

int get_data(int *a, int size) {
  int cnt;
  char tmp = ' ';

  printf("Introduza los elementos del array separados por espacios, <enter> para terminar:\n");
  for (cnt = 0; cnt < size && tmp != '\n'; cnt++) {
    scanf("%d", &a[cnt]);
    scanf("%c", &tmp);
  }

  while (tmp != '\n') {
    scanf("%c", &tmp);
  }

  return cnt;
}

void foo(int *a1, int *a2, int size) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    a2[cnt] = a1[cnt] % 2 == 0? powf(a1[cnt], 2): sqrtf(a1[cnt]);
}

void print_data(int *a, int size) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    printf("%s%d%s", (cnt == 0?"[":""), a[cnt], (cnt == size - 1?"]\n":", ") );
}

int main() {
  const int MAX = 20;
  int size;
  int array1[MAX], array2[MAX];

  size = get_data(array1, MAX);
  foo(array1, array2, size);

  printf("Array original:   ");
  print_data(array1, size);
  printf("Array resultante: ");
  print_data(array2, size);

  return 0;
}
