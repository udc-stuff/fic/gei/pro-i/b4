#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

void init_random_array(int *a, int size) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    a[cnt] = random();
}

void print_data(int *a, int size) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    printf("%s%d%s", (cnt == 0?"[":""), a[cnt], (cnt == size - 1?"]\n":", ") );
}

void join(int *input1, int *input2, int *output, int input_size) {
  int cnt;

  for (cnt = 0; cnt < input_size; cnt++) {
    output[2*cnt] = input1[cnt];
    output[2*cnt + 1] = input2[cnt];
  }

}

void swap(int *a, int *b) {
   int tmp = *a;
   *a = *b;
   *b = tmp;
}

void bubble_sort(int *a, int size) { 
   int cnt, aux; 
   bool swapped; 

   for (cnt = 0; cnt < size - 1; cnt++) { 
     swapped = false; 
     for (aux = 0; aux < size - cnt - 1; aux++) { 
        if (a[aux] > a[aux + 1]) { 
           swap(&a[aux], &a[aux+1]); 
           swapped = true; 
        } 
     } 
  
     // IF no two elements were swapped by inner loop, then break 
     if (swapped == false) 
        break; 
   } 
} 

int main() {
  const int SIZE = 15;
  int array1[SIZE], array2[SIZE], array3[2 * SIZE];

  srandom(time(NULL));
  init_random_array(array1, SIZE);
  init_random_array(array2, SIZE);

  printf("El primer array de numeros es:  ");
  print_data(array1, SIZE);
  printf("El segundo array de numeros es: ");
  print_data(array2, SIZE);

  join(array1, array2, array3, SIZE);
  bubble_sort(array3, SIZE * 2);

  printf("El array resultante ordenado es: ");
  print_data(array3, SIZE * 2);

  return 0;
}
