#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void init_random_array(int *a, int size) {
  int cnt;

  srand(time(NULL));

  for (cnt = 0; cnt < size; cnt++)
    a[cnt] = rand() % size;
}

void print_data(int *a, int size) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    printf("%s%d%s", (cnt == 0?"[":""), a[cnt], (cnt == size - 1?"]\n":", ") );
}

int recursive_sum(int *a, int size, int pos) {
  if (pos + 1 == size)
    return a[pos];

  return a[pos] + recursive_sum(a, size, pos + 1);
}

int main() {
  const int SIZE = 5;
  int array[SIZE];

  init_random_array(array, SIZE);

  printf("El array de numeros es: ");
  print_data(array, SIZE);

  printf("La suma de los elementos es %d\n", recursive_sum(array, SIZE, 0));


  return 0;
}
