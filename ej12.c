#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

void print_data(int *a, int size) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    printf("%s%d%s", (cnt == 0?"[":""), a[cnt], (cnt == size - 1?"]\n":", ") );
}

void init_random_array(int *a, int size) {
  int cnt;

  srand(time(NULL));

  for (cnt = 0; cnt < size; cnt++)
    a[cnt] = rand() % size;
}

void init_random_reversible_number(int *a, int size) {
  int cnt, aux;
  aux = size / 2 + (size % 2 == 0? 0: 1);

  init_random_array(a, aux);

  for (cnt = 0; cnt < aux; cnt++)
    a[size - cnt - 1] = a[cnt];
}

bool is_reversible_number(int *a, int size) {
  int cnt;

  for (cnt = 0; cnt < size/2; cnt++) {
    //printf("a[%d] = %d   a[%d] = %d\n", cnt, a[cnt], size - cnt - 1, a[size - cnt -1]);
    if (a[cnt] != a[size - cnt - 1])
      return false;
  }

  return true;
}

void print_result(int *a, int size) {
  print_data(a, size);
  printf(
    "El array %s capicua\n\n",
    (is_reversible_number(a, size) ? "es" : "no es")
  );
}

int main() {
  const int SIZE1 = 5;
  const int SIZE2 = 8;
  const int SIZE3 = 7;
  const int SIZE4 = 10;
  
  int array1[SIZE1];
  int array2[SIZE2];
  int array3[SIZE3];
  int array4[SIZE4];

  init_random_array(array1, SIZE1);
  init_random_array(array2, SIZE2);

  init_random_reversible_number(array3, SIZE3);
  init_random_reversible_number(array4, SIZE4);


  print_result(array1, SIZE1);
  print_result(array2, SIZE2);
  print_result(array3, SIZE3);
  print_result(array4, SIZE4);

  return 0;
}
