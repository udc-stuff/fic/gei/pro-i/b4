#include <stdio.h>
#include <stdbool.h>
#include <string.h>

/**
 * Funcion que dentro de una matriz de tamano sizexsize muestra la submatriz que
 * esta formada en las posiciones m[start:start+subsize-1][start:start+subsize-1]
 * @param size: tamano original de la submatriz
 * @param start: indice mas bajo de la submatriz a mostrar
 * @param subsize: tamanno de la submatriz
 */
void print_matrix(int size, int m[size][size], int start, int subsize) {
  int r, c;
  int max = start + subsize;

  for (r = start; r < max; r++)
    for (c = start; c < max; c++)
      printf("%3d%s"
      , m[r][c], (c + 1 != max ? ", ": "\n" )

    );

}

/**
 * Funcion que solicita datos para rellenar los valores de una submatriz
 * donde la matriz original tiene el tamano sizexsize. La sumbatriz esta
 * formada en las posiciones m[start:start+subsize-1][start:start+subsize-1]
 * @param size: tamano original de la submatriz
 * @param start: indice mas bajo de la submatriz a mostrar
 * @param subsize: tamanno de la submatriz
 */
void get_data(int size, int m[size][size], int start, int subsize) {
  int r, c;
  int max = start + subsize;

  for (r = start; r < max; r++) {
    for (c = start; c < max; c++) {
      printf("Introduce los valores de la posicion %d,%d: ", r, c);
      scanf("%d", &m[r][c]);
    }
    printf("\n");
  }
}

/**
 * Funcion que solicita al usuario un numero que tiene que estar entre dos valores
 * @param min: valor minimo que puede introducir el usuario (inclusive)
 * @param max: valor maximo que puede introducir el usuario (inclusive)
 */
int get_int_between_min_max(int min, int max) {
  int aux = min -1;

  while (aux < min || aux > max) {
    printf("Introduce un numero que este en el rango [%d, %d]: ", min, max);
    scanf("%d", &aux);
  }

  return aux;
}

bool get_yes_no(char *msg, char *yes, char *no) {
  const int N = 3;
  char tmp[N];

  while (true) {
    printf("%s (%s/%s): ", msg, yes, no);
    scanf("%s", tmp);

    if (strcmp(tmp, yes) == 0) {
      return true;
    } else if (strcmp(tmp, no) == 0) {
      return false;
    } else {
      printf("Opciones no validas. Introduzca '%s' o '%s'.\n", yes, no);
    }

  }

  // El codigo nunca llega a esta linea
  // Esta linea esta puesta para que algunos compiladores no tengan
  // un mensaje de warning
  return false;
}

int min(int a, int b) {
  return (a < b)? a: b;
}

int max(int a, int b) {
  return (a > b)? a: b;
}

/**
 * Funcion que busca el valor maximo/minimo dentro de los valores de una submatriz
 * donde la matriz original tiene el tamano sizexsize. La sumbatriz esta
 * formada en las posiciones m[start:start+subsize-1][start:start+subsize-1]
 * @param size: tamano original de la submatriz
 * @param start: indice mas bajo de la submatriz a mostrar
 * @param subsize: tamanno de la submatriz
 * @param f: funcion que escoge un numero entre dos
 */
int get_number_by_condition(int size, int m[size][size], int start, int subsize,
                            int (*f)(int, int)) {

  int r, c;
  int aux = m[start][start];
  int max = start + subsize;

  for (r = start; r < max; r++)
    for (c = start; c < max; c++)
      aux = f(aux, m[r][c]);

  return aux;
}

/**
 * Funcion para introducir valores de manera consecutiva en una matriz
 */
void fill_matrix(int size, int m[size][size]) {
  int r, c, cnt;

  for (cnt = r = 0; r < size; r++)
    for (c = 0; c < size; c++)
      m[r][c] = ++cnt;
}

int main() {
  const int N = 20;
  int matrix[N][N];
  int size;
  bool stop = false;

  //fill_matrix(N, matrix);

  while (!stop) {
    size = get_int_between_min_max(1, N);
    get_data(N, matrix, 0, size);
    print_matrix(N, matrix, 0, size);
    printf("El valor mas grande de la matriz es %d\n", get_number_by_condition(N, matrix, 0, size, max));
    printf("El valor mas pequeno de la matriz es %d\n", get_number_by_condition(N, matrix, 0, size, min));
    stop = get_yes_no("Desea parar la ejecucion?", "Si", "No");
  }
}
