#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void init_random_int_matrix(int rows, int columns, int m[rows][columns]) {
  int r, c;

  for (r = 0; r < rows; r++)
    for (c = 0; c < columns; c++)
      m[r][c] = random() % (rows * columns);
}

void init_random_float_matrix(int rows, int columns, float m[rows][columns]) {
  int r, c;

  for (r = 0; r < rows; r++)
    for (c = 0; c < columns; c++)
      m[r][c] = random() % (rows * columns) + ((float) random())/RAND_MAX;
}


void print_int_matrix(int rows, int columns, int m[rows][columns]) {
  int r, c;

  for (r = 0; r < rows; r++)
    for (c = 0; c < columns; c++)
      printf("%d%s", m[r][c], (c == columns - 1?"\n":"\t") );
}

void print_float_matrix(int rows, int columns, float m[rows][columns]) {
  int r, c;

  for (r = 0; r < rows; r++)
    for (c = 0; c < columns; c++)
      printf("%f%s", m[r][c], (c == columns - 1?"\n":"\t") );
}

void transpose_int_matrix(int size, int input[size][size], int output[size][size]) {
  int r, c;

  for (r = 0; r < size; r++)
    for (c = 0; c < size; c++)
      output[c][r] = input[r][c];
}

void transpose_float_matrix(int size, float input[size][size], float output[size][size]) {
  int r, c;

  for (r = 0; r < size; r++)
    for (c = 0; c < size; c++)
      output[c][r] = input[r][c];
}

int main() {
  const int SIZE = 5;
  int int_matrix[SIZE][SIZE], transposed_int_matrix[SIZE][SIZE];
  float float_matrix[SIZE][SIZE], transposed_float_matrix[SIZE][SIZE];

  srandom(time(NULL));

  init_random_int_matrix(SIZE, SIZE, int_matrix);
  init_random_float_matrix(SIZE, SIZE, float_matrix);

  transpose_int_matrix(SIZE, int_matrix, transposed_int_matrix);
  transpose_float_matrix(SIZE, float_matrix, transposed_float_matrix);

  printf("Matriz de enteros original: \n");
  print_int_matrix(SIZE, SIZE, int_matrix);

  printf("Matriz de enteros traspuesta: \n");
  print_int_matrix(SIZE, SIZE, transposed_int_matrix);

  printf("\n\n\n");
  printf("Matriz de reales original: \n");
  print_float_matrix(SIZE, SIZE, float_matrix);

  printf("Matriz de reales traspuesta: \n");
  print_float_matrix(SIZE, SIZE, transposed_float_matrix);

  return 0;
}
