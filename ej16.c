#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

void init_matrix(int rows, int columns, int m[rows][columns]) {
  int r, c, cnt;

  for (cnt = r = 0; r < rows; r++)
    for (c = 0; c < columns; c++)
      m[r][c] = ++cnt;
}


void print_matrix(int rows, int columns, int m[rows][columns]) {
  int r, c;

  for (r = 0; r < rows; r++)
    for (c = 0; c < columns; c++)
      printf("%d%s", m[r][c], (c == columns - 1?"\n":"\t") );
}

void swap(int *a, int *b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

void mess(int rows, int columns, int m[rows][columns]) {
  int r1, r2, c1, c2;
  int cnt, times;

  times = rows * columns;
  for (cnt = 0; cnt < times; cnt++) {
    r1 = random() % rows;
    r2 = random() % rows;
    c1 = random() % columns;
    c2 = random() % columns;
    swap(&m[r1][c1], &m[r2][c2]);
  }
}

bool is_magic_matrix(int size, int m[size][size]) {
  int r, c;
  int aux, tmp_r, tmp_c;

  // compronar que tiene los numeros de 1 a size*size
  int a[size*size];
  for (aux = 0; aux < size*size; aux++)
    a[aux] = 0;

  for (r = 0; r < size; r++)
    for (c = 0; c < size; c++)
      if (m[r][c] < 1 || m[r][c] > size*size)
        return false;
      else
        a[m[r][c]]++;

  for (aux = 0; aux < size*size; aux++)
    if (a[aux] != 1)
      return false;


  // suma de una fila
  aux = 0;
  for (c = 0; c < size; c++)
    aux += m[0][c];

  // comprobar que las filas y las columnas suman lo mismo
  for (r = 0; r < size; r++) {
    tmp_r = tmp_c = 0;
    for (c = 0; c < size; c++) {
      tmp_r += m[r][c];
      tmp_c += m[c][r];
    }
    if (aux != tmp_r || aux != tmp_c)
      return false;
  }

  // diagonales
  for (r = 0; r < size; r++) {
    tmp_r += m[r][r];
    tmp_c += m[r][size-r-1];
  }

  return aux != tmp_r || aux != tmp_c;
}

int main() {
  const int SIZE = 3;
  int matrix[SIZE][SIZE];
  //int matrix[SIZE][SIZE] = {8, 1, 6, 3, 5, 7, 4, 9, 2};

  srandom(time(NULL));

  init_matrix(SIZE, SIZE, matrix);
  mess(SIZE, SIZE, matrix);

  print_matrix(SIZE, SIZE, matrix);
  printf("La matriz %s cuadrado magico\n", is_magic_matrix(SIZE, matrix)? "es": "no es");


  return 0;
}
