#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void init_random(int size, int a[]) {
  int cnt;
  int max = size * 10;

  for (cnt = 0; cnt < size; cnt++)
    a[cnt] = rand() % max;
}

void print_array(int size, int a[]) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    printf("%s%d%s", (cnt == 0?"[":""), a[cnt], (cnt == size - 1?"]\n":", ") );
}

/**
 * Funcion para usar en qsort
 * devuelve 0 si a y b son iguales
 * devuelve un numero >0 si a es mayor que b
 * devuelve un numero <0 si b es mayor que a
 */
int compare(const void *a, const void *b) {
	int *a1 = (int *) a;
	int *b1 = (int *) b;

	return *a1 - *b1;
}

void join(int size, int a1[], int a2[], int a3[], int big[]) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++) {
    big[cnt] = a1[cnt];
    big[cnt + size] = a2[cnt];
    big[cnt + 2*size] = a3[cnt];
  }
}


int main() {
  const int N = 5;
  int array1[N];
  int array2[N];
  int array3[N];

  int big_array[3*N];

  // Inicializando la semilla para los numeros aleatorios
  //srand(time(NULL)); // No repicable
  srand(0); // Repicable

  init_random(N, array1);
  init_random(N, array2);
  init_random(N, array3);

  printf("Primer array: ");
  print_array(N, array1);
  printf("Segundo array: ");
  print_array(N, array2);
  printf("Tercer array: ");
  print_array(N, array3);

  join(N, array1, array2, array3, big_array);

  printf("El array con todos los numeros es: ");
  print_array(3*N, big_array);

  qsort(big_array, 3*N, sizeof(int), compare);

  printf("El array ordenado es: ");
  print_array(3*N, big_array);


  return 0;
}
