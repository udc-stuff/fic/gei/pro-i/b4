#include <stdio.h>

struct student {
  char name[20];
  float mark;
};

void get_data(int size, struct student students[]) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++) {
    printf("\nEstudiante %d:\n", cnt);

    printf("\tIntroduce el nombre del estudiante: ");
    scanf("%s", students[cnt].name);

    printf("\tIntroduce la nota del estudiante: ");
    scanf("%f", &students[cnt].mark);
  }
}

void print_student(struct student student) {
  printf("Student {name: %s, mark: %.2f}", student.name, student.mark);
}

void print_array(int size, struct student students[]) {
  int cnt;

  printf("[");
  for (cnt = 0; cnt < size; cnt++) {
    print_student(students[cnt]);
    if (cnt + 1 != size)
      printf(", ");
  }
  printf("]");
}

int get_best(int size, struct student students[]) {
  int cnt, aux = 0;

  for (cnt = 1; cnt < size; cnt++) {
    if (students[cnt].mark > students[aux].mark)
      aux = cnt;
  }

  return aux;
}

int get_worst(int size, struct student students[]) {
  int cnt, aux = 0;

  for (cnt = 1; cnt < size; cnt++) {
    if (students[cnt].mark < students[aux].mark)
      aux = cnt;
  }

  return aux;
}

int main() {
  const int STUDENTS = 15;
  struct student students[STUDENTS];
  int best, worst;

  get_data(STUDENTS, students);

  best = get_best(STUDENTS, students);
  worst = get_worst(STUDENTS, students);


  print_array(STUDENTS, students);
  printf("\n");

  printf("El mejor estudiante esta en la posicion %d y es: ", best);
  print_student(students[best]);
  printf("\n");

  printf("El peor estudiante esta en la posicion %d y es: ", worst);
  print_student(students[worst]);
  printf("\n");
}
