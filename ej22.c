#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void init_random(int size, float a[]) {
  int cnt;
  int max = size * 10;

  for (cnt = 0; cnt < size; cnt++)
    a[cnt] = random() % max + ((float) random()/ RAND_MAX);
}

void print_array(int size, float a[]) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    printf("%s%.5f%s", (cnt == 0?"[":""), a[cnt], (cnt == size - 1?"]\n":", ") );
}

/**
 * Funcion para usar en qsort
 * devuelve 0 si a y b son iguales
 * devuelve un numero >0 si a es mayor que b
 * devuelve un numero <0 si b es mayor que a
 */
int compare(const void *a, const void *b) {
	float *a1 = (float *) a;
	float *b1 = (float *) b;

	return *a1 - *b1;
}

void join(int size, float a1[], float a2[], float big[]) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++) {
    big[cnt] = a1[cnt];
    big[cnt + size] = a2[cnt];
  }
}


int main() {
  const int N = 30;
  float array1[N];
  float array2[N];

  float big_array[2*N];

  // Inicializando la semilla para los numeros aleatorios
  //srandom(time(NULL)); // No repicable
  srandom(0); // Repicable

  init_random(N, array1);
  init_random(N, array2);

  printf("Primer array: ");
  print_array(N, array1);
  printf("Segundo array: ");
  print_array(N, array2);

  join(N, array1, array2, big_array);

  printf("El array con todos los numeros es: ");
  print_array(2*N, big_array);

  qsort(big_array, 2*N, sizeof(float), compare);

  printf("El array ordenado es: ");
  print_array(2*N, big_array);

  return 0;
}
