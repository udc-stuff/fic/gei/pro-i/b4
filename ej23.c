#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void init_random(int size, char a[]) {
  int cnt;
  int max = size * 10;

  for (cnt = 0; cnt < size; cnt++) {
    switch (random() % 3) {
      case 0:
        a[cnt] = '0' + random() % ('9' - '0');
        break;
      case 1:
        a[cnt] = 'A' + random() % ('A' - 'Z');
        break;
      case 2:
        a[cnt] = 'a' + random() % ('a' - 'z');
        break;
    } 
  }
}

void print_array(int size, char a[]) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    printf("%s%c%s", (cnt == 0?"[":""), a[cnt], (cnt == size - 1?"]\n":", ") );
}

/**
 * Funcion para usar en qsort
 * devuelve 0 si a y b son iguales
 * devuelve un numero >0 si a es mayor que b
 * devuelve un numero <0 si b es mayor que a
 */
int compare(const void *a, const void *b) {
	char *a1 = (char *) a;
	char *b1 = (char *) b;

	return *a1 - *b1;
}

void join(int size, char a1[], char a2[], char big[]) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++) {
    big[cnt] = a1[cnt];
    big[cnt + size] = a2[cnt];
  }
}


int main() {
  const int N = 100;
  char array1[N];
  char array2[N];

  char big_array[2*N];

  // Inicializando la semilla para los numeros aleatorios
  //srandom(time(NULL)); // No repicable
  srandom(0); // Repicable

  init_random(N, array1);
  init_random(N, array2);

  printf("Primer array: ");
  print_array(N, array1);
  printf("Segundo array: ");
  print_array(N, array2);

  join(N, array1, array2, big_array);

  printf("El array con todos los numeros es: ");
  print_array(2*N, big_array);

  qsort(big_array, 2*N, sizeof(char), compare);

  printf("El array ordenado es: ");
  print_array(2*N, big_array);

  return 0;
}
