#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void stats(char *s) {
  int cnt;
  int vowels[] = {0, 0, 0, 0, 0};
  char tmp[] = {'a', 'e', 'i', 'o', 'u'};

  for (cnt = 0; cnt < strlen(s); cnt++)
    switch (s[cnt]) {
      case 'a':
      case 'A':
        vowels[0]++;
        break;
      case 'e':
      case 'E':
        vowels[1]++;
        break;
      case 'i':
      case 'I':
        vowels[2]++;
        break;
      case 'o':
      case 'O':
        vowels[3]++;
        break;
      case 'u':
      case 'U':
        vowels[4]++;
        break;
    }
  
  for (cnt = 0; cnt < 5; cnt++)
    printf("La letra %c aparece %d veces (%.0f%%)\n", tmp[cnt], vowels[cnt], 100 * ((float) vowels[cnt])/strlen(s)); 

}

int main() {
  const int MAX = 200;
  char str[MAX];  
  char *ptr;

  printf("Introduce una frase: ");
  fgets(str, MAX, stdin);

  if ((ptr = strchr(str, '\n')) != NULL)
	*ptr = '\0';

  stats(str);

  return 0;
}
