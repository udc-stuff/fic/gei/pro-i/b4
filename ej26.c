#include <stdio.h>
#include <string.h>

int min(int a, int b) {
  return a < b? a: b;
}

void replace(int size, char result[size], char *txt, char *word, char *synonym) {
  int cnt, aux;

  size--; // Esto esta puesto porque el caracter \0 ocupa un espacio

  result[0] = '\0'; // Empezamos poniendo que el resultado es una cadena vacia
  int start = 0;

  for (cnt = 0; cnt <= strlen(txt) - strlen(word); cnt++) {
    if (txt[cnt] != word[0])
      continue;

      // En este punto txt[cnt] tiene la primera letra de word
      if (strncmp(word, &txt[cnt], strlen(word)) == 0) {

        //printf("\t (%d, %ld => %d)\n",cnt - start, size - strlen(result), min(cnt - start, size - strlen(result)));

        if (min(cnt - start, size - strlen(result)) <= 0) // Miramos que se puede anadir texto
          return;

        strncat(result, &txt[start], min(cnt - start, size - strlen(result))); // copiamos el trozo de texto que no es la palabra a buscar
        strncat(result, synonym, size - strlen(result)); // Anadimos al final el sinomimo
        start = cnt +  strlen(word);
      }
  }

  strncat(result, &txt[start], size - strlen(result)); // copiamos el trozo que hay despues del sinonimo


}

int main() {
  const int N = 100;
  char txt[] = {"El cielo es azul y blanco. Lo que tambien es azul es el mar."};
  char word[] = {"azul"};
  char syn[] = {"negro"};
  char result[N];

  printf("Quiero cambiar la palabra <%s> por <%s> dentro del texto <%s>\n", word, syn, txt);

  replace(N, result, txt, word, syn);

  printf("El resultado es <%s>\n", result);

  return 0;
}
