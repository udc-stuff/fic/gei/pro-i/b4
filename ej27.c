#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void foo(char *s) {
  char delim[] = {" ,.\n"};
  char *token;

  while ((token = strsep(&s, delim)) != NULL) {
    if (strcmp(token, "")) {
      printf("%s\t%lu\n", token, strlen(token));
    }
  }

}

int main() {
  const int MAX = 100;
  char str[MAX];

  printf("Introduce una frase de como maximo %d caracteres:\n", MAX);
  fgets(str, MAX, stdin);

  foo(str);

  return 0;
}
