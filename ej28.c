#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void foo(char *s) {
  char *ptr = s;
  int cnt, start;

  while ((ptr = strchr(ptr, ' ')) != NULL) {
    ptr++; // to check the next characters

    for (cnt = 0; ptr[cnt] == ' '; cnt++)
       ;

    if (cnt != 0) {
      start = cnt;
      for (cnt = 0; ptr[cnt+start] != '\0'; cnt++)
        ptr[cnt] = ptr[cnt+start];
      ptr[cnt] = '\0';
    }

  }

  printf("La frase final es: %s\n", s);
}

int main() {
  const int MAX = 100;
  char str[MAX];

  printf("Introduce una frase de como maximo %d caracteres:\n", MAX);
  fgets(str, MAX, stdin);

  foo(str);

  return 0;
}
