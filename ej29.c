#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *reverse(char *s) {
  int cnt, size;
  char tmp;

  size = strlen(s);
  for (cnt = 0 ; cnt < size/2; cnt++) {
    tmp = s[cnt];
    s[cnt] = s[size - cnt - 1];
    s[size - cnt - 1] = tmp;
  }

  return s;
}

int main() {
  const int MAX = 100;
  char str[MAX];
  char *ptr;

  printf("Introduce una frase: ");
  fgets(str, MAX, stdin);

  if ((ptr = strchr(str, '\n')) != NULL)
    *ptr = '\0';

  printf("La frase al reves es <%s>\n", reverse(str));

  return 0;
}


