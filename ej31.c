#include <stdio.h>
#include <stdbool.h>
#include <string.h>

/**
 * Funcion que comprueba que en los primeros n caracteres de una cadena
 * se encuentra un numero real
 */
bool isnreal(char *str, int n) {
  int cnt;
  bool dot = false; // Para controlar si ya a aparecido un punto
  bool has_number = false; // Para controlar si hay algun numero

  if (str == NULL || n <= 0)
    return false;

  //El simbolo negativo si aparece tiene que estar al principio
  cnt = (str[0] == '-' ? 1: 0);

  for (; cnt < n; cnt++) {
    if (str[cnt] == '\0')
      break;

    if (str[cnt] < '0' || str[cnt] > '9') { // si no es un numero
      if (str[cnt] != '.') // si no es un punto decimal
        return false;

      // Si el codigo llega aqui el caracter es un punto decimal
      // Como solo puede existir un punto decimal hay que comprobar
      // si ya se encontro antes
      if (dot)
        return false;

      dot = true;
    } else {
      // Si entra aqui es que el caracter es un numero
      has_number = true;
    }
  }

  return has_number;
}

/**
 * Funcion que comprueba que la cadena de caracteres esta compuesta por un
 * numero real
 */
bool isreal(char *str) {
  if (str == NULL)
    return false;

  return isnreal(str, strlen(str));
}

void foo(char *str) {
  printf("La cadena %s tiene un numero real? %s\n",
          str, (isreal(str)? "True": "False" ) );
  printf("La cadena %s tiene en los %ld primeros caracteres un numero real? %s\n",
          str, strlen(str), (isnreal(str, strlen(str))? "True": "False" ) );
  printf("\n");
}

int main() {
  int cnt;
  char strings[][40] = {
    "-3", "3", ".3", "-.3", "3.", "-3.",
    "-1234", "1234", ".4322", "-.1234", "1234.", "-1234.",
    ".", "-",  "-.", ".-",
    "3-", "-3-", "3-3", "--3",
    "1234..", ".3.", "1.2.3.4", "1.2-3.4",
    "-3.a", "A", "3,4",
    "" // Esta para saber donde debe para el bucle
  };

  char str[] = {"-23.56789abc"};

  for (cnt = 0; strlen(strings[cnt]) != 0; cnt++)
    foo(strings[cnt]);

  printf("\n\n");

  for (cnt = 0; cnt < strlen(str) != 0; cnt++) {
    printf("La cadena %s tiene en los primeros %d caracteres un numero real?\n", str, cnt);
    printf("%s\n\n", (isnreal(str, cnt)? "True": "False" ));
  }

  printf("\n\n");

  printf("La cadena NULL es un numero real?\n%s\n\n", (isreal(NULL)? "True": "False" ));



  return 0;
}
