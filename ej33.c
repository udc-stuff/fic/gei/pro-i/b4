#include <stdio.h>

int main() {
  char c;
  int digits, letters, others;

  digits = letters = others = 0;
  printf("Introduce una frase: ");
  while (c != '\n') {
    scanf("%c", &c);
    if (c >= '0' && c <= '9')
      digits++;
    else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
      letters++;
    else
      others++;
  }

  printf("Letras:  %d\n", letters);
  printf("Digitos: %d\n", digits);
  printf("Otros:   %d\n", others);

  return 0;
}
