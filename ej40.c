#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

void init_board(int N, int board[N][N]) {
	int r, c;

	//srand(time(NULL));
	srand(0);

	for (r = 0; r < N; r++)
		for (c = 0; c < N; c++) {
			if (r == 0 || c == 0)
				board[r][c] = 0;
			else if (r == N -1 || c == N -1)
				board[r][c] = 0;
			else
				board[r][c] = (rand() % 10) == 0;
		}

}

void print_board(int N, int board[N][N]) {
        int r, c;

  // Las decenas
	printf("  ");
	for (c = 0; c < N; c++)
		printf("%2d", c / 10);
	printf("\n");
	// Las unidades
	printf("  ");
	for (c = 0; c < N; c++)
		printf("%2d", c % 10);
	printf("\n");

  for (r = 0; r < N; r++) {
		printf("%2d)", r);
    for (c = 0; c < N; c++) {
			printf("%c%c",
					(board[r][c] == 0? ' ': 'X'),
					(c == N -1 ? '\n': ' ')
			);
		}
	}
}

int neighbors(int N, int board[N][N], int x, int y) {
	int r, c;
	int total = 0;

//	printf("\n\tCalculando los vecinos de %d y %d\n", x, y);
	for (r = -1; r <= 1; r++) {
		for (c = -1; c <= 1; c++) {
//			printf("\t\t%d,%d\n", x+r, y+c );
			total += board[x+r][y+c];
		}
	}

	total = total -board[x][y];
//	printf("%d,%d>%d\n", x, y, total);
	return total; // uno no puede ser vecino de si mismo

}

void next(int N, int board[N][N]) {
	int tmp_board[N][N];
	int r, c, aux;

	for (r = 1; r < N-1; r++) {
		for (c = 1; c < N-1; c++) {
			aux = neighbors(N, board, r, c);
			switch (aux) {
				case 2:
					if (board[r][c] == 0) {
						tmp_board[r][c] = 0;
						break;
					}
				case 3:
					tmp_board[r][c] = 1;
					break;
				default:
					tmp_board[r][c] = 0;
			}
//			printf("v:%d r:%d c:%d new%d old%d\n", aux, r, c, tmp_board[r][c], board[r][c]);
		}
	}

	for (r = 1; r < N-1; r++)
		for (c = 1; c < N-1; c++)
			board[r][c] = tmp_board[r][c];
}

bool is_empty(int N, int board[][N]) {
	int r, c;

	for (r = 1; r < N-1; r++)
                for (c = 1; c < N-1; c++)
			if (board[r][c] != 0)
				return false;

	return true;

}

void press_enter(char *msg) {
	char aux;

	printf("%s", msg);
	aux = getchar();
	while (aux != '\n')
		aux = getchar();
}

void custom_wait() {
	time_t tmp = time(NULL);
	struct tm *t1 = localtime(&tmp);

	int sec = t1->tm_sec;
	while (sec == t1->tm_sec) {
		tmp = time(NULL);
		t1 = localtime(&tmp);
	}
}

int main() {
	const int N = 70;
	int board[N][N];

	init_board(N, board);
	print_board(N, board);

	while (!is_empty(N, board)) {
		custom_wait();
		//press_enter("Pulse enter");

		next(N, board);
		print_board(N, board);
	}

}
